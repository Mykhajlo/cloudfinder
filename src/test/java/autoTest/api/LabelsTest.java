package autoTest.api;

import app.controllers.AuthController;
import app.controllers.GmailLabelController;
import app.utils.Config;
import com.sun.org.glassfish.gmbal.Description;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class LabelsTest {

    private static final String NAME = UUID.randomUUID().toString();
    private static final String RESPONCE_LABEL_NAME = "name";
    private static final String RESPONCE_LABEL_ID = "id";
    private static String labelId = "";
    private static final String USER_ID = Config.getProperty("account.userId");
    private static final String AUTH_TOKEN = AuthController.getAccessToken();

    @Description("Create label test")
    @Test
    @Order(1)
    public void createLabelTest() {

        String body = generateAddLabelRequestBody();

        String addLabelResponse = GmailLabelController.addLabel(USER_ID, AUTH_TOKEN, body)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .asString();

        assertFalse(addLabelResponse.isEmpty(), "Unexpected response");
        String actualLabelName = getLabelNameResponseByKey(addLabelResponse, RESPONCE_LABEL_NAME);
        assertEquals(actualLabelName, NAME,"Unexpected Name");
        labelId = getLabelNameResponseByKey(addLabelResponse, RESPONCE_LABEL_ID);
        assertFalse(labelId.isEmpty());
    }

    @Description("Delete label test")
    @Test
    @Order(2)
    public void deleteLabelTest() {

        String deleteLabelResponse = GmailLabelController.deleteLabel(USER_ID, AUTH_TOKEN, labelId)
                .then()
                .statusCode(HttpStatus.SC_NO_CONTENT)
                .extract()
                .asString();

        assertTrue(deleteLabelResponse.isEmpty(), "Unexpected response");
    }

    private String generateAddLabelRequestBody() {
        return new JSONObject()
                .put("labelListVisibility", "labelShow")
                .put("messageListVisibility", "show")
                .put("name", NAME)
                .toString();
    }

    private static String getLabelNameResponseByKey(String addLabelResponse, String key) {
        return new JSONObject(addLabelResponse)
                .getString(key);
    }
}
