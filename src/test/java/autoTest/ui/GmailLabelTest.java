package autoTest.ui;


import app.pages.app.pages.GmailPage;
import app.pages.app.pages.LoginPage;
import app.utils.Config;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

import static app.utils.Utils.setupEnvironment;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by mykhail on 06.12.2019.
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GmailLabelTest {

    private static Logger logger = LoggerFactory.getLogger(GmailLabelTest.class);
    private static WebDriver driver;
    private static final String MAIL = Config.getProperty("account.userId");
    private static final String PASSWORD = Config.getProperty("account.password");
    private static final String LABEL_NAME = UUID.randomUUID().toString();

    private static GmailPage gmail;

    @BeforeAll
    public static void setUp() {

        driver = setupEnvironment();
        logger.info("Test is started");

        LoginPage login = new LoginPage(driver);
        login.enterMail(MAIL);
        login.clickMailNext();
        login.enterPassword(PASSWORD);
        gmail = login.clickPasswordNext();

    }

    @AfterAll
    public static void cleanUp() {
        driver.manage().deleteAllCookies();
        driver.close();
        logger.info("Test is finished");
    }

    @Test
    @Order(1)
    @DisplayName("Create label test")
    public void createLabelTest()  {

        logger.info("Create label test is started");
        gmail.clickMoreButton();
        gmail.clickNewLabel();
        gmail.enterLabelName(LABEL_NAME);
        gmail.clickSaveLabelButton();

        assertNotNull(gmail.getLabelByName(LABEL_NAME));
        logger.info("Create label test is finished");
    }

    @Test
    @Order(2)
    @DisplayName("Delete label test")
    public void deleteLabelTest()  {
        logger.info("Delete label test is started");

        gmail.openLabelSideMenu(LABEL_NAME);
        gmail.clickDeleteLabel();
        gmail.submitLabelDelete();

        assertNull(gmail.getLabelByName(LABEL_NAME));

        logger.info("Delete label test is finished");
    }
}
