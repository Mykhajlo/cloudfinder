package app.controllers;

import app.enums.Uri;
import app.services.Request;
import app.services.RequestService;
import io.restassured.http.ContentType;
import io.restassured.response.Response;


public class GmailLabelController {

    private GmailLabelController() {};

    public static Response addLabel(String userId, String authToken, String body){
        String source = String.format("/users/%s/labels", userId);

        Request request = new Request.RequestBuilder()
                .setUri(Uri.GOOGLE_GMAIL_API.getUri())
                .setPath(source)
                .setContentType(ContentType.JSON.toString())
                .setAuthToken(authToken)
                .setBody(body)
                .build();

        return RequestService.postOperation(request);
    }

    public static Response deleteLabel(String userId, String authToken, String labelId){
        String source = String.format("/users/%s/labels/%s", userId, labelId);

        Request request = new Request.RequestBuilder()
                .setUri(Uri.GOOGLE_GMAIL_API.getUri())
                .setPath(source)
                .setContentType(ContentType.JSON.toString())
                .setAuthToken(authToken)
                .build();

        return RequestService.deleteOperation(request);
    }
}
