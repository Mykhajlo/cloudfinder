package app.controllers;

import app.utils.Config;

public class AuthController {

    static final String URL = Config.getProperty("auth.url");
    private AuthController() {}

    public static String getAccessToken(){
        return Config.getProperty("auth.accessToken");
    }

//    public static Response getAccessToken(){
//        String source = "/token";
//
//        Request request = new Request.RequestBuilder()
//                .setUri(URL)
//                .setPath(source)
//                .setHeaders(getAccessTokenHeaders())
//                .setBody(generateGetAccessTokenBody())
//                .build();
//
//        return RequestService.postOperation(request);
//    }

}
