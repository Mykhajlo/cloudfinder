package app.services;

import io.restassured.http.ContentType;

import java.util.HashMap;
import java.util.Map;


public class Request {

    private String uri;
    private String path;
    private String contentType;
    private String body;
    private String authToken;
    private Map<String,String> queryParams;
    private Map<String,String> headers;

    public static class RequestBuilder {
        private static final String EMPTY_STRING = "";

        private String uri = EMPTY_STRING;
        private String path = EMPTY_STRING;
        private Map<String,String> queryParams = new HashMap<String,String>();
        private String contentType = ContentType.JSON.toString();
        private String body = EMPTY_STRING;
        private String authToken = EMPTY_STRING;
        private Map<String,String> headers = new HashMap<String,String>();


        public RequestBuilder() {
            queryParams.put(EMPTY_STRING, null);
        }

        public RequestBuilder setUri(String uri){
            this.uri = uri;
            return this;
        }

        public RequestBuilder setPath(String path){
            this.path = path;
            return this;
        }

        public RequestBuilder setContentType(String contentType){
            this.contentType = contentType;
            return this;
        }

        public RequestBuilder setBody (String body){
            this.body = body;
            return this;
        }

        public RequestBuilder setAuthToken (String authToken) {
            this.authToken = authToken;
            return this;
        }

        public RequestBuilder setQueryParam (Map<String,String> queryParams){
            this.queryParams = queryParams;
            return this;
        }

        public RequestBuilder setHeaders (Map<String,String> headers){
            this.headers = headers;
            return this;
        }

        public Request build() {
            return new Request(uri, path, queryParams, contentType, headers, authToken, body);
        }
    }

    public Request(String uri, String path, Map<String,String> queryParams, String contentType, Map<String,String> headers, String authToken, String body) {
        this.uri = uri;
        this.path = path;
        this.queryParams = queryParams;
        this.contentType = contentType;
        this.headers = headers;
        this.authToken = authToken;
        this.body = body;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public Map<String,String> getQueryParams() {
        return queryParams;
    }

    public void setQueryParams(Map<String,String> queryParams) {
        this.queryParams = queryParams;
    }

    public Map<String,String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String,String> headers) {
        this.headers = headers;
    }

}

