package app.services;

import io.restassured.authentication.OAuthSignature;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class RequestService {
    public static Response getOperation(Request request) {
        return given()
                .baseUri(request.getUri())
                .basePath(request.getPath())
                .queryParams(request.getQueryParams())
                .contentType(request.getContentType())
                .auth().oauth2(request.getAuthToken())
                .body(request.getBody())
                .redirects().follow(true)
                .when()
                .get();
    }

    public static Response postOperation(Request request) {
        return given()
                .baseUri(request.getUri())
                .basePath(request.getPath())
                .queryParams(request.getQueryParams())
                .contentType(request.getContentType())
                .headers(request.getHeaders())
                .auth().oauth2(request.getAuthToken())
                .body(request.getBody())
                .when()
                .post();
    }

    public static Response deleteOperation(Request request) {
        return given()
                .baseUri(request.getUri())
                .basePath(request.getPath())
                .queryParams(request.getQueryParams())
                .contentType(request.getContentType())
                .headers(request.getHeaders())
                .auth().oauth2(request.getAuthToken())
                .body(request.getBody())
                .when()
                .delete();
    }
}
