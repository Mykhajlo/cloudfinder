package app.enums;

import app.utils.Config;

public enum Uri {
    GOOGLE_GMAIL_API(Config.getProperty("google.mail.api")),
    GOOGLE_GMAIL_URL(Config.getProperty("google.mail.url"));

    private String uri;

    Uri(String envUrl) {
        this.uri = envUrl;
    }

    public String getUri() {
        return uri;
    }
}

