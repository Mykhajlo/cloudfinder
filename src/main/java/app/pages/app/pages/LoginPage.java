package app.pages.app.pages;

import app.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


public class LoginPage extends BasePage{

    public LoginPage (WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "[type='email']")
    private WebElement mailField;

    @FindBy(css = "[id='identifierNext']")
    private WebElement mailNextButton;

    @FindBy(css = "[type='password']")
    private WebElement passwordField;

    @FindBy(css = "[id='passwordNext']")
    private WebElement passwordNextButton;

    public  void enterMail (String mail){
        WebDriverWait wait =  new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(mailField));
        mailField.clear();
        mailField.sendKeys(mail);
    }

    public void clickMailNext(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(mailNextButton));
        mailNextButton.click();
    }

    public  void enterPassword (String pass){
        WebDriverWait wait =  new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(passwordField));
        passwordField.clear();
        passwordField.sendKeys(pass);
    }

    public GmailPage clickPasswordNext()  {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(passwordNextButton));
        passwordNextButton.click();
        return new GmailPage(driver);
    }
}
