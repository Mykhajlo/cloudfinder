package app.pages.app.pages;

import app.pages.BasePage;
import app.utils.JavaScriptUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

public class GmailPage extends BasePage {

    private static Logger logger = LoggerFactory.getLogger(GmailPage.class);

    public GmailPage (WebDriver driver) {
        super(driver);
    }

    static final String LABEL_SELECTOR_PATTERN = "//span[@class='nU ']//a[@title='%s']";

    @FindBy(css = "[class='CJ']")
    private WebElement moreButton;

    @FindBy(css = "[class='CL Wj']")
    private WebElement createNewLabel;

    @FindBy(xpath = "//div[@role='alertdialog']//input[@class='xx']")
    private WebElement labelNameField;

    @FindBy(xpath = "//div[@role='alertdialog']//button[@name='ok']")
    private WebElement saveButton;

    @FindBy(xpath = "/html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div[1]/div/div[2]/div[13]/div")
    private WebElement deletLabel;

    @FindBy(xpath = "//div[@class='Kj-JD-Jl']//button[@name='ok']")
    private WebElement submitDeleteLabel;

    @FindBy(xpath = "//span[@class='aT']//span[@class='bAq']")
    private WebElement infoMessage;

    public void clickMoreButton(){
        JavaScriptUtils.scrollToWebElement(driver, moreButton);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(moreButton));
        moreButton.click();
    }

    public void clickNewLabel(){
        JavaScriptUtils.scrollToWebElement(driver, createNewLabel);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(createNewLabel));
        createNewLabel.click();
    }

    public void enterLabelName (String name)  {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(labelNameField));
        labelNameField.clear();
        labelNameField.sendKeys(name);
    }

    public void clickSaveLabelButton(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(saveButton));
        saveButton.click();
    }

    public void openLabelSideMenu(String name){

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement labelName = driver.findElement(By.xpath("//span[@class='nU ']//a[@title='"+ name +"']"));
        JavaScriptUtils.scrollToWebElement(driver, labelName);

        Actions actions = new Actions(driver);

        actions.moveToElement(labelName).build().perform();
        WebElement labelMenu = driver.findElement(By.cssSelector("[data-label-name='"+ name +"']"));
        wait.until(ExpectedConditions.elementToBeClickable(labelMenu));
        labelMenu.click();
    }

    public void clickDeleteLabel() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(deletLabel));
        deletLabel.click();
    }

    public void submitLabelDelete()   {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10), Duration.ofSeconds(3));
        wait.until(ExpectedConditions.visibilityOf(submitDeleteLabel));
        submitDeleteLabel.click();

        wait.until(ExpectedConditions.visibilityOf(infoMessage));
    }

    public WebElement getLabelByName(String name){
        try {
            return driver.findElement(By.xpath(String.format(LABEL_SELECTOR_PATTERN, name)));
        } catch(NoSuchElementException e){
            logger.info(e.getMessage());
            return null;
        }

    }
}
