package app.utils;


import org.openqa.selenium.InvalidArgumentException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {
    private Config() {}

    public static String getProperty(String propertyName) {
        try (InputStream input = new FileInputStream("configs/common.properties")) {

            Properties prop = new Properties();
            prop.load(input);

            return prop.getProperty(propertyName);

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        throw new InvalidArgumentException("Failed to get property: " + propertyName);
    }
}
