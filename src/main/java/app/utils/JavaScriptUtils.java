package app.utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class JavaScriptUtils {
    public static void scrollToWebElement(WebDriver driver, WebElement element){
        final String SCROLLING_SCRIPT = "arguments[0].scrollIntoView();";
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript(SCROLLING_SCRIPT, element);
    }
}
