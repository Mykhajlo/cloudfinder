package app.utils;

import app.enums.Uri;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class Utils {
    public static ChromeDriver setupEnvironment() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        System.setProperty("webdriver.chrome.driver", "/Users/mykhail/IdeaProjects/сloudfinder/src/main/resources/drivers/chromedriver");
        ChromeDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().setSize(new Dimension(1400,900));
        driver.get(Uri.GOOGLE_GMAIL_URL.getUri());
        return driver;
    }
}
