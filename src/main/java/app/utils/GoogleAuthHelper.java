package app.utils;

import app.enums.Uri;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.json.JSONObject;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public final class GoogleAuthHelper {

    /**
     * Please provide a value for the CLIENT_ID constant before proceeding, set this up at https://code.google.com/apis/console/
     */
    private static final String CLIENT_ID = Config.getProperty("account.clientId");
    /**
     * Please provide a value for the CLIENT_SECRET constant before proceeding, set this up at https://code.google.com/apis/console/
     */
    private static final String CLIENT_SECRET = Config.getProperty("account.clientSecret");

    /**
     * Callback URI that google will redirect to after successful authentication
     */
    private static final String CALLBACK_URI = Config.getProperty("auth.callbackUrl");

    // start google authentication constants
    private static final List<String> SCOPE = Collections.singletonList(Config.getProperty("auth.labelsScope"));
    private static final String USER_INFO_URL = Uri.GOOGLE_GMAIL_API.getUri();
    private static final JsonFactory JSON_FACTORY = new JacksonFactory();
    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    // end google authentication constants

    private String stateToken;

    private final GoogleAuthorizationCodeFlow flow;

    /**
     * Constructor initializes the Google Authorization Code Flow with CLIENT ID, SECRET, and SCOPE
     */
    public GoogleAuthHelper() {
        flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT,
                JSON_FACTORY, CLIENT_ID, CLIENT_SECRET, SCOPE).build();
        generateStateToken();
    }

    /**
     * Builds a login URL based on client ID, secret, callback URI, and scope
     */
    public String buildLoginUrl() {

        final GoogleAuthorizationCodeRequestUrl url = flow.newAuthorizationUrl();

        return url.setRedirectUri(CALLBACK_URI).setState(stateToken).build();
    }

    /**
     * Generates a secure state token
     */
    private void generateStateToken(){

        SecureRandom sr1 = new SecureRandom();

        stateToken = "google;"+sr1.nextInt();

    }

    /**
     * Accessor for state token
     */
    public String getStateToken(){
        return stateToken;
    }

    /**
     * Expects an Authentication Code, and makes an authenticated request for the user's profile information
     * @return JSON formatted user profile information
     * @param authCode authentication code provided by google
     */
    public String getUserInfoJson(final String authCode) throws IOException {

        final GoogleTokenResponse response = flow.newTokenRequest(authCode).setRedirectUri(CALLBACK_URI).execute();
        final Credential credential = flow.createAndStoreCredential(response, null);
        final HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory(credential);
        // Make an authenticated request

        String source = String.format("/users/%s/labels", Config.getProperty("account.userId"));
        final GenericUrl url = new GenericUrl(USER_INFO_URL + source);

        HttpContent content = new JsonHttpContent(JSON_FACTORY,generateAddLabelRequestBody());
        HttpRequest postLabel = requestFactory.buildPostRequest(url,content);

        postLabel.getHeaders().setContentType("application/json");
        final String jsonIdentity = postLabel.execute().parseAsString();

//        final HttpRequest request = requestFactory.buildGetRequest(url);
//        request.getHeaders().setContentType("application/json");
//        final String jsonIdentity = request.execute().parseAsString();

        return jsonIdentity;

    }

    private String generateAddLabelRequestBody() {
        return new JSONObject()
                .put("labelListVisibility", "labelShow")
                .put("messageListVisibility", "show")
                .put("name", UUID.randomUUID().toString())
                .toString();
    }
}
